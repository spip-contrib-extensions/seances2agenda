<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin
 *
 * @package SPIP\Seances2Agenda\Installation
**/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation de seances2agenda : migration vers Agenda
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 */
function seances2agenda_upgrade($nom_meta_base_version, $version_cible) {
	// Création des tables
	include_spip('base/abstract_sql');
	include_spip('action/editer_objet');
	include_spip('action/editer_mot');

	$maj = [];
	$maj['create'] = [
		['seances2agenda_importer_endroits'],
		['seances2agenda_importer_seances']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

// Migration des endroits des Séances dans un groupe de mots clef : _Séances_Endroits_
function seances2agenda_importer_endroits() {
	// Tout ça n'a de sens que si les tables du plugin Séances sont là
	$trouver_table = charger_fonction('trouver_table', 'base');
	if ($trouver_table('spip_seances_endroits')) {
		// 1 - Créer le groupe _Séances_Endroits_
		
		// a - Vérifier si le groupe existe déjà (en récupérant son id)
		$id_groupe_endroits = seances2agenda_id_groupe('_Séances_Endroits_');
		
		// b - S'il n'existe pas, le créer et récupérer son id
		if (!$id_groupe_endroits) {
			$set = [
				'titre' => '_Séances_Endroits_',
				'descriptif' => '',
				'texte' => '',
				'unseul' => 'oui',
				'obligatoire' => 'oui',
				'tables_liees' => 'evenements',
				'minirezo' => 'oui',
				'comite' => 'oui',
				'forum' => 'non'
			];	
			$id_groupe_endroits = objet_inserer('groupe_mots', null, $set);
		}
		
		// 2 - Créer les mots pour chaque endroits
		$endroits = sql_allfetsel('*', 'spip_seances_endroits');
		foreach ($endroits as $endroit) {
			// a - Vérifier si le mot existe déjà
			$id_mot = seances2agenda_id_mot($endroit['nom_endroit'], $id_groupe_endroits);
			// b - S'il n'existe pas, le créer
			if (!$id_mot) {
				$set = [
					'titre' => $endroit['nom_endroit'],
					'descriptif' => $endroit['descriptif_endroit'],
					'texte' => '',
					'id_groupe' => $id_groupe_endroits,
					'type' => 'spip_seances_endroits'
				];
				$id_mot = objet_inserer('mot', $id_groupe_endroits, $set);
			}
		}
	}
}

// Migration des séances en Événéments dans l'Agenda
function seances2agenda_importer_seances() {
	// Tout ça n'a de sens que si les tables du plugin Séances sont là
	$trouver_table = charger_fonction('trouver_table', 'base');
	if ($trouver_table('spip_seances')) {
		$id_groupe_endroits = seances2agenda_id_groupe('_Séances_Endroits_');
		// 1 - Créer les événements
		$seances = sql_allfetsel('*', 'spip_seances');
		foreach ($seances as $seance) {
			// a - Vérifier si l'événement existe déjà
			$article = seances2agenda_article($seance['id_article']);
			$id_evenement = seances2agenda_id_evenement($article['titre'], $seance['id_article'], $seance['date_seance']);
			
			// b - S'il n'existe pas, le créer
			if (!$id_evenement) {
				$set = [
					'id_article' => $seance['id_article'],
					'titre' => $article['titre'],
					'descriptif' => $seance['remarque_seance'],
					'date_debut' => $seance['date_seance'],
					'date_fin' => $seance['date_seance'],
					'statut' => $article['statut']
				];
				$id_evenement = objet_inserer('evenement', $seance['id_article'], $set);
			}
			
			// c - Lier au mot clef en fonction de l'endroit
			//  * trouver le titre de l'endroit
			$titre_endroit = sql_fetsel('nom_endroit', 'spip_seances_endroits', 'id_endroit=' . $seance['id_endroit']);
			
			//  * trouver le mot correspondant
			$id_mot_endroit = seances2agenda_id_mot($titre_endroit, $id_groupe_endroits);
			
			//  * le lier à l'événement
			$set = [
				'evenement' => $id_evenement
			];
			mot_associer($id_mot_endroit, $set);
		}
	}
}

// fonction pour trouver l'id du groupe de mots clés à partir du titre du groupe
function seances2agenda_id_groupe($titre) {
	$titre = sql_quote($titre);
	$result = sql_fetsel('id_groupe', 'spip_groupes_mots', "titre=$titre");
	$resultat = ($result['id_groupe'] ?? false);
	return $resultat;
}

//fonction qui permet de trouver l'id du mot clé à partir du titre et de l'id du groupe
function seances2agenda_id_mot($titre, $id_groupe) {
	$titre = sql_quote($titre);
	$result = sql_fetsel(
		'id_mot',
		'spip_mots',
		"titre=$titre AND id_groupe=$id_groupe"
	);
	$id_mot = ($result['id_mot'] ?? false);
	return $id_mot;
}

// fonction pour trouver l'id de l'événement
function seances2agenda_id_evenement($titre, $id_article, $date_debut) {
	$titre = sql_quote($titre);
	$date_debut = sql_quote($date_debut);
	$result = sql_fetsel('id_evenement', 'spip_evenements', "titre=$titre AND id_article=$id_article AND date_debut=$date_debut");
	$resultat = ($result['id_evenement'] ?? false);
	return $resultat;
}

// fonction pour trouver le titre de l'article
function seances2agenda_article($id_article) {
	$titre = sql_quote($titre);
	$result = sql_fetsel('titre, statut', 'spip_articles', "id_article=$id_article");
	return $result;
}


/**
 * Désinstallation/suppression des tables de seances2agenda
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 */
function seances2agenda_vider_tables($nom_meta_base_version) {
	include_spip('inc/meta');
	effacer_meta($nom_meta_base_version);
}
