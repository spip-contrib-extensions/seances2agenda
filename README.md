# Séances2Agenda

Un plugin pour migrer le contenu du plugin Séances vers Agenda.

## Usage
* il faut avoir le plugin Agenda et PHP 7
* activer le plugin Séances2Agenda
* patienter
* désinstaller le plugin Séances2Agenda, il a fait son travail, il ne sert plus à rien
* admirer le résultat dans `ecrire/?exec=evenements`
* mettre à jour les squelettes en conséquence